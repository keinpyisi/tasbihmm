package com.kpshkk.tasbihmm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class SendDataService extends Service {
    private final LocalBinder mBinder = new LocalBinder();
    protected Handler handler;
    protected Toast mToast;
    public static final String inputFormat = "HH:mm";

    private static Date date;
    private static Date dateCompareOne;
    private static Date dateCompareTwo;
    static Boolean check1=false;
    static  Boolean check2=false;


    static SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat);

    public class LocalBinder extends Binder {
        public SendDataService getService() {
            return SendDataService .this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {


        super.onCreate();

        final Handler h = new Handler();
        final int delay = 5000; // milliseconds

        h.postDelayed(new Runnable() {
            public void run() {
                // do something


                    new Thread(new Runnable() {
                        @RequiresApi(api = Build.VERSION_CODES.O)
                        @Override
                        public void run() {
                            // SOME CODE
                            Date date = new Date();

                            // Date date = new Date();   // given date
                            Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
                            calendar.setTime(date);   // assigns calendar to given date
                            int hournow= calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
//                            int minutenow= calendar.get(Calendar.MINUTE);        // gets hour in 12h format
//                            int milisecnow= calendar.get(Calendar.MILLISECOND);       // gets month number, NOTE this is zero based!




                            if ( hournow==5 || hournow==18  ) {
                                if(!check1){
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                                        startMyOwnForeground();
                                        stopForeground(false);
                                   //     startMyOwnForeground();
                                    }

                                    else{
                                        String NOTIFICATION_CHANNEL_ID = "com.kpshkk.tasbihmm";
                                        String channelName = "My Background Service";
                                        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
                                        chan.setLightColor(Color.BLUE);
                                        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
                                        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                        assert manager != null;
                                        manager.createNotificationChannel(chan);

                                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(SendDataService.this, NOTIFICATION_CHANNEL_ID);
                                        Notification notification = notificationBuilder.setOngoing(true)
                                                .setSmallIcon(R.mipmap.ic_launcher_foreground)
                                                .setContentTitle("Notification")
                                                .setContentText("It's time to do Dhikr (Zikir)")
                                                .setOngoing(false)
                                                .setCategory(Notification.CATEGORY_SERVICE)
                                                .build();

                                        startForeground(1, notification);
                                        stopForeground(false);
                                        check1=true;
                                    }


                                }


                            }else{

                                check1=false;
                            }




                        }
                    }).start();

                h.postDelayed(this, delay);
            }
        }, delay);







    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startMyOwnForeground(){
        String NOTIFICATION_CHANNEL_ID = "com.kpshkk.tasbihmm";
        String channelName = "My Background Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.mipmap.ic_launcher_foreground)
                .setContentTitle("Notification")
                .setContentText("It's time to do Dhikr (Zikir)")
                .setOngoing(false)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();

        startForeground(2, notification);






    }
    // Remove notification
    private void removeNotification() {
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(2);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Context context=this;
        handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {




// write your code to post content on server
            }
        });

        return android.app.Service.START_STICKY;
    }
}
