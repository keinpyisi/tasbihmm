package com.kpshkk.tasbihmm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "HistoryDB";
    private static final String TABLE_CONTACTS = "history";
    private static final String KEY_ID = "id";
    private static final String KEY_DATE = "date";
    private static final String KEY_DHIKRID = "dhikrid";
    private static final String KEY_COUNT = "count";
    private static final String KEY_TIME = "time";
    private static final String KEY_CHECK = "clickreset";


    // private String date,id,dhikrid,count,time;
    //    private boolean clickreset;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_DATE + " TEXT," + KEY_DHIKRID + " TEXT," + KEY_COUNT + " TEXT,"
                + KEY_CHECK + " BOOLEAN,"
                + KEY_TIME + " TEXT" + ")";

        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

        // Create tables again
        onCreate(db);
    }

    // code to add the new contact
    void addHistory(History contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DATE, contact.getDate()); // Contact Name
        values.put(KEY_DHIKRID, contact.getDhikrid()); // Contact Phone
        values.put(KEY_COUNT, contact.getCount()); // Contact Phone
        values.put(KEY_CHECK, contact.isClickreset()); // Contact Phone
        values.put(KEY_TIME, contact.getTime()); // Contact Phone

        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    // Getting contacts Count
    public int getHistoryCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
       // cursor.close();

        // return count
        return cursor.getCount();
    }

    public ArrayList<History> getHistoryofSameDate(String dhikr) {
        ArrayList<History> dbdata = new ArrayList<>();
        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        SQLiteDatabase db = this.getReadableDatabase();


        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_CONTACTS+" WHERE "+KEY_DHIKRID+"=?" +" AND " + KEY_DATE+"=?"+" AND " + KEY_CHECK+"=0", new String[] {dhikr,formattedDate});


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                History contact = new History();
                //  contact.setId();
                contact.setId(cursor.getString(0));
                contact.setDate(cursor.getString(1));
                contact.setDhikrid(cursor.getString(2));
                contact.setCount(cursor.getString(3));
                contact.setClickreset(Boolean.getBoolean(cursor.getString(4)));
                contact.setTime(cursor.getString(5));

                // Adding contact to list
                dbdata.add(contact);
            } while (cursor.moveToNext());
        }
        return dbdata;
    }





    // code to get all contacts in a list view
    public ArrayList<History> getAllHistorySize() {
        ArrayList<History> contactList = new ArrayList<History>();
        // Select All Query
        String selectQuery = "SELECT  Distinct dhikrid,count,time,clickreset,date FROM " + TABLE_CONTACTS+ " ORDER BY " + "date"+ " DESC , time DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                History contact = new History();

                contact.setDhikrid(cursor.getString(0));
                contact.setCount(cursor.getString(1));
                contact.setClickreset(Boolean.getBoolean(cursor.getString(3)));
                contact.setTime(cursor.getString(2));
                contact.setDate(cursor.getString(4));

                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

    // code to update the single contact
    public int updateContact(History contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DATE, contact.getDate()); // Contact Name
        values.put(KEY_DHIKRID, contact.getDhikrid()); // Contact Phone
        values.put(KEY_COUNT, contact.getCount()); // Contact Phone
        values.put(KEY_CHECK, contact.isClickreset()); // Contact Phone
        values.put(KEY_TIME, contact.getTime()); // Contact Phone
        values.put(KEY_ID, contact.getId()); // Contact Phone


        // updating row
        return db.update(TABLE_CONTACTS, values, KEY_ID + " = "+contact.getId(),
                null);
    }



}
