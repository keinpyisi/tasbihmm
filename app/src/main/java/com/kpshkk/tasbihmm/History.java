package com.kpshkk.tasbihmm;

public class History {
    private String date,id,dhikrid,count,time;
    private boolean clickreset;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDhikrid() {
        return dhikrid;
    }

    public void setDhikrid(String dhikrid) {
        this.dhikrid = dhikrid;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public boolean isClickreset() {
        return clickreset;
    }

    public void setClickreset(boolean clickreset) {
        this.clickreset = clickreset;
    }
}
