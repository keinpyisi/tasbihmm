package com.kpshkk.tasbihmm;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;

import com.opencsv.CSVReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CustomAdaptor extends ArrayAdapter<DataModel> {
    private ArrayList<DataModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView no;
        TextView myan;
        TextView arab;
        ImageView arrow;
        View view;
          }

    public CustomAdaptor(ArrayList<DataModel> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext=context;

    }


    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        DataModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);


            viewHolder.no = (TextView) convertView.findViewById(R.id.num);
            viewHolder.myan = (TextView) convertView.findViewById(R.id.burmese);
            viewHolder.arab = (TextView) convertView.findViewById(R.id.arab);
            viewHolder.arrow=(ImageView)convertView.findViewById(R.id.arrowbtn);

          viewHolder.view=(View)convertView.findViewById(R.id.view);

            if((position+1)!=dataSet.size()){
                viewHolder.arrow.setImageResource(R.drawable.ic_baseline_arrow_forward_ios_24);

            }else{
                viewHolder.arrow.setImageResource(R.drawable.ic_baseline_arrow_forward_ios_24);


            }
            result=convertView;



            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(dataModel.getCondition().toLowerCase()=="asma"){
                   Vibrator   va= (Vibrator) mContext.getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            va.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));

                        } else {
                            //deprecated in API 26
                            va.vibrate(100);
                        }
                        ViewHolder viewHolder = (ViewHolder) v.getTag();



                        try {
                            InputStream ins = mContext.getApplicationContext().getResources().openRawResource(
                                    mContext.getApplicationContext().getResources().getIdentifier("asma",
                                            "raw", mContext.getApplicationContext().getPackageName()));
                            ///new CSVReader(new InputStreamReader(myInputStream, encoding));

                            CSVReader reader = new CSVReader(new InputStreamReader(ins, "UTF-8"));
                            List<String[]> allLines = reader.readAll();

                            String no=viewHolder.no.getText().toString().trim();
                            DataModel dataModelss=new DataModel(allLines.get(Integer.parseInt(no)-1)[0]+" ", ""+allLines.get(Integer.parseInt(no)-1)[2], allLines.get(Integer.parseInt(no)-1)[1],allLines.get(Integer.parseInt(no)-1)[3],"asma");
                            // 2. Confirmation message
                            // create an alert builder
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                            // set the custom layout
                            final View customLayout = inflater.inflate(R.layout.custom_layout, null);
                            Button closebtn= customLayout.findViewById(R.id.confirm_button);
                            TextView myan_text= customLayout.findViewById(R.id.myan_text);
                            myan_text.setText(dataModelss.getArab().replace("n", System.getProperty("line.separator")));
                            TextView myande_text=customLayout.findViewById(R.id.myande_text);
                            myande_text.setText(dataModelss.getMyan().replace("n", System.getProperty("line.separator")));
                            TextView arab_text= customLayout.findViewById(R.id.arab_text);
                            arab_text.setText(dataModelss.getDetarab().replace("n", System.getProperty("line.separator")));
                            builder.setView(customLayout);


                            // create and show the alert dialog
                            AlertDialog dialog = builder.create();
                            dialog.show();
                            closebtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        va.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));

                                    } else {
                                        //deprecated in API 26
                                        va.vibrate(100);
                                    }
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                            // Toast.makeText(this, "The specified file was not found", Toast.LENGTH_SHORT).show();
                        }






                    }else if(dataModel.getCondition().toLowerCase().equals("dhikr")){

                        ViewHolder viewHolder = (ViewHolder) v.getTag();

                        try {
                            InputStream ins = mContext.getApplicationContext().getResources().openRawResource(
                                    mContext.getApplicationContext().getResources().getIdentifier("dhikr",
                                            "raw", mContext.getApplicationContext().getPackageName()));
                            ///new CSVReader(new InputStreamReader(myInputStream, encoding));

                            CSVReader reader = new CSVReader(new InputStreamReader(ins, "UTF-8"));
                            List<String[]> allLines = reader.readAll();

                            String no=viewHolder.no.getText().toString().trim();
                            // 2. Confirmation message
                            // create an alert builder
                            ((MainActivity)mContext).detailOpen(allLines.get(Integer.parseInt(no)-1)[1],allLines.get(Integer.parseInt(no)-1)[4],allLines.get(Integer.parseInt(no)-1)[5]);
                        } catch (Exception e) {
                            e.printStackTrace();
                            // Toast.makeText(this, "The specified file was not found", Toast.LENGTH_SHORT).show();
                        }


                    }else if(dataModel.getCondition().toLowerCase().equals("nafl")){
                     Vibrator   va= (Vibrator) mContext.getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            va.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));

                        } else {
                            //deprecated in API 26
                            va.vibrate(100);
                        }
                        ViewHolder viewHolder = (ViewHolder) v.getTag();
                        String no=viewHolder.no.getText().toString().trim();
                        ((MainActivity)mContext).pdfopen(no);
                    }

                }
            });
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.no.setText(dataModel.getNo().replace("n", System.getProperty("line.separator")));
        viewHolder.myan.setText(dataModel.getMyan().replace("n", System.getProperty("line.separator")));
        viewHolder.arab.setText(dataModel.getArab().replace("n", System.getProperty("line.separator")));
   //    convertView.setOnClickListener(this);
     //  convertView.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }

}
