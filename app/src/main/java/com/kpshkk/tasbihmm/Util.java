package com.kpshkk.tasbihmm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Util {
    public static final String inputFormat = "HH:mm";

    private static Date date;
    private static Date dateCompareOne;
    private static Date dateCompareTwo;
static Boolean check1=false;
  static  Boolean check2=false;


    static SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat);
    // schedule the start of the service every 10 - 30 seconds
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void scheduleJob(Context context) {

        Date date = new Date();

       // Date date = new Date();   // given date
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(date);   // assigns calendar to given date
       int hournow= calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
       int minutenow= calendar.get(Calendar.MINUTE);        // gets hour in 12h format
       int milisecnow= calendar.get(Calendar.MILLISECOND);       // gets month number, NOTE this is zero based!

if(minutenow==59){
    check1=false;
    check2=false;
}

        if ( hournow==4 && minutenow==58) {
            if(!check1){
                //yada yada
                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                int notificationId = 1;
                String channelId = "channel-01";
                String channelName = "Channel Name";
                int importance = NotificationManager.IMPORTANCE_HIGH;

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    NotificationChannel mChannel = new NotificationChannel(
                            channelId, channelName, importance);
                    notificationManager.createNotificationChannel(mChannel);
                }

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Notification")
                        .setContentText("It's time to do Dhikr (Zikir)");

                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
               // Intent intent=new Intent(context,NotificationPublisher.class);
                Intent intent=new Intent(context,MainActivity.class);

                stackBuilder.addNextIntent(intent);
                PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
                mBuilder.setContentIntent(resultPendingIntent);

                notificationManager.notify(notificationId, mBuilder.build());
check1=true;
            }


        }
        if ( hournow==18 && minutenow==58) {
            if(!check2){
                //yada yada
                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                int notificationId = 1;
                String channelId = "channel-01";
                String channelName = "Channel Name";
                int importance = NotificationManager.IMPORTANCE_HIGH;

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    NotificationChannel mChannel = new NotificationChannel(
                            channelId, channelName, importance);
                    notificationManager.createNotificationChannel(mChannel);
                }

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Notification")
                        .setContentText("It's time to do Dhikr (Zikir)");

                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
               // Intent intent=new Intent(context,NotificationPublisher.class);
                Intent intent=new Intent(context,MainActivity.class);

                stackBuilder.addNextIntent(intent);
                PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
                mBuilder.setContentIntent(resultPendingIntent);

                notificationManager.notify(notificationId, mBuilder.build());
                check2=true;
            }


        }


        //ComponentName serviceComponent = new ComponentName(context, TestJobService.class);
        ComponentName serviceComponent = new ComponentName(context, MainActivity.class);

        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
        builder.setMinimumLatency(1 * 1000); // wait at least
        builder.setOverrideDeadline(3 * 1000); // maximum delay
        //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
        //builder.setRequiresDeviceIdle(true); // device should be idle
        //builder.setRequiresCharging(false); // we don't care if the device is charging or not
        JobScheduler jobScheduler = context.getApplicationContext().getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
    }
    private static Date parseDate(String date) {

        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }
}
