package com.kpshkk.tasbihmm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class HistoryAdaptor extends RecyclerView.Adapter<HistoryAdaptor.HistoryHolder>{
    // List to store all the contact details
    private ArrayList<History> contactsList;
    private Context mContext;

    // Counstructor for the Class
    public HistoryAdaptor(ArrayList<History> contactsList, Context context) {
        this.contactsList = contactsList;
        this.mContext = context;
    }

    // This method creates views for the RecyclerView by inflating the layout
    // Into the viewHolders which helps to display the items in the RecyclerView
    @Override
    public HistoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        // Inflate the layout view you have created for the list rows here
        View view = layoutInflater.inflate(R.layout.onehistory, parent, false);
        return new HistoryHolder(view);
    }

    @Override
    public int getItemCount() {
        return contactsList == null? 0: contactsList.size();
    }

    // This method is called when binding the data to the views being created in RecyclerView
    @Override
    public void onBindViewHolder(@NonNull HistoryHolder holder, final int position) {

        final History contact = contactsList.get(position);

        // Set the data to the views here
//        holder.setContactName(contact.getName());
//        holder.setContactNumber(contact.getNumber());
holder.hisdate.setText(contact.getDate());
holder.hiscount.setText(contact.getCount());
holder.hismyan.setText(contact.getId().replace("n", System.getProperty("line.separator")));
holder.hisarab.setText(contact.getDhikrid());

        // You can set click listners to indvidual items in the viewholder here
        // make sure you pass down the listner or make the Data members of the viewHolder public

    }

    // This is your ViewHolder class that helps to populate data to the view
    public class HistoryHolder extends RecyclerView.ViewHolder {

        private TextView hisarab;
        private TextView hismyan,hiscount,hisdate;

        public HistoryHolder(View itemView) {
            super(itemView);

//            txtName = itemView.findViewById(R.id.txt_name);
//            txtNumber = itemView.findViewById(R.id.txt_number);
            hisarab=itemView.findViewById(R.id.hisarab);
            hismyan=itemView.findViewById(R.id.hismyan);
            hiscount=itemView.findViewById(R.id.hiscount);
            hisdate=itemView.findViewById(R.id.hisdate);
        }


    }
}
