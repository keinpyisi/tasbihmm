package com.kpshkk.tasbihmm;

import android.view.View;

public class DataModel {
    private String no,myan,arab,detarab,condition;

private View view;

    public DataModel(String no, String myan, String arab,String detarab,String condition) {
        this.no = no;
        this.myan = myan;
        this.arab = arab;
        this.detarab=detarab;
        this.condition=condition;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getDetarab() {
        return detarab;
    }

    public void setDetarab(String detarab) {
        this.detarab = detarab;
    }

    public DataModel(View view) {
        this.view = view;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getMyan() {
        return myan;
    }

    public void setMyan(String myan) {
        this.myan = myan;
    }

    public String getArab() {
        return arab;
    }

    public void setArab(String arab) {
        this.arab = arab;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }
}
