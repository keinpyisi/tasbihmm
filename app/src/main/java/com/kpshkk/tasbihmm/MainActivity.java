package com.kpshkk.tasbihmm;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.opencsv.CSVReader;
import com.skydoves.powerspinner.OnSpinnerItemSelectedListener;
import com.skydoves.powerspinner.PowerSpinnerView;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class MainActivity extends AppCompatActivity {
    public static final String MyPREFERENCES = "HKK" ;
    ImageButton plusbtn,minusbtn,resetbtn,menubtn,historybtn;
    private HistoryAdaptor listAdapter;
    private ArrayList<History> contactsList = new ArrayList<>();
    List<String> spinnerdata=new ArrayList<String>();
    List<String> myandata=new ArrayList<String>();
    View pdfview;
    private RecyclerView historyrecycle;
    TextView countct,titl,subtitl;
    int count,preLast;
    DrawerLayout drawer;
    NavigationView navview;
    SharedPreferences sharedpreferences;
    String datas,spinnerID,con2;
    View friend_request_item;

    RelativeLayout mainlayout,sublayout,detaillayout,hissublayout,pdflayout;
    ImageView crossclose;
    Vibrator va;

    ArrayList<DataModel> dataModels;
    ListView listView;
    PowerSpinnerView spinner;
    private static CustomAdaptor adapter;
    int indexSpinner=0;
    ArrayList<String>datatemp=new ArrayList<>();
    History history = new History();
    private ReviewManager reviewManager;
    private  boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_NOTIFICATION_POLICY);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.VIBRATE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.VIBRATE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_NOTIFICATION_POLICY);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),1);
            return false;
        }
        return true;
    }
    // Shows the app rate dialog box using In-App review API
    // The app rate dialog box might or might not shown depending
    // on the Quotas and limitations
    public void showRateApp() {
        com.google.android.play.core.tasks.Task<ReviewInfo> request = reviewManager.requestReviewFlow();
        request.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                // Getting the ReviewInfo object
                ReviewInfo reviewInfo = task.getResult();

                com.google.android.play.core.tasks.Task<Void> flow = reviewManager.launchReviewFlow(this, reviewInfo);
                flow.addOnCompleteListener(task1 -> {
                    // The flow has finished. The API does not indicate whether the user
                    // reviewed or not, or even whether the review dialog was shown.
                    Toast.makeText(MainActivity.this,"Thanks you for Rating.",Toast.LENGTH_LONG).show();
                });
            }else{
                Toast.makeText(MainActivity.this,"Sorry Rate Failed....",Toast.LENGTH_LONG).show();
            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        // The request code used in ActivityCompat.requestPermissions()
// and returned in the Activity's onRequestPermissionsResult()
        checkAndRequestPermissions();
        if(!NotificationManagerCompat.from(MainActivity.this).areNotificationsEnabled()){
            Toast.makeText(MainActivity.this,"Please Turn On Notification in Setting.",Toast.LENGTH_LONG);
        }


        reviewManager = ReviewManagerFactory.create(this);
        DatabaseHandler dbs= new DatabaseHandler(this);
        //


//Initiate Firebase
        FirebaseApp.initializeApp(MainActivity.this);
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();

                        Intent serviceIntent = new Intent(MainActivity.this, MyFirebaseMessagingService.class);
                        serviceIntent.putExtra("UserID", token);
                        startService(serviceIntent);

                    }
                });
        ///Initiate Ads
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {


            }
        });
        AdView  mAdViewssa = findViewById(R.id.ads);

        AdRequest adRequestssa = new AdRequest.Builder().build();
        mAdViewssa.loadAd(adRequestssa);
//InitiateAds
        //ScheduleAds
        scheduleNotification();
        //ScheduleAds

        //SpinnerID Initiate
        spinnerID=0+"";
        //Initiate
        va= (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        datas=sharedpreferences.getString("data", "0"); // getting String
        drawer=findViewById(R.id.drawer_layout);
        navview=findViewById(R.id.navigation);
        hissublayout=findViewById(R.id.hissublayout);
        pdflayout=findViewById(R.id.pdflayout);
        historybtn=findViewById(R.id.historybtn);
        spinner = findViewById(R.id.nice_spinner);


//Hiatory Button
        historybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                } else {
                    //deprecated in API 26
                    va.vibrate(50);
                }
                contactsList.clear();
                spinner.dismiss();
                hissublayout.removeAllViews();
                LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                // set the custom layout
                final View child = inflater.inflate(R.layout.historylayout, null);
                ImageButton  menubtntwo=child.findViewById(R.id.menubtn);
                menubtntwo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        spinner.dismiss();
                        drawer.openDrawer(GravityCompat.START);
                    }
                });
                ImageButton arrowtwo=child.findViewById(R.id.arrowtwo);
                arrowtwo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                        } else {
                            //deprecated in API 26
                            va.vibrate(50);
                        }
                        spinner.dismiss();
                        hissublayout.setVisibility(View.INVISIBLE);
                        mainlayout.setVisibility(View.VISIBLE);

                    }
                });
                //Initiate
                historyrecycle=child.findViewById(R.id.historyrecycle);

                ArrayList<History>alldata=dbs.getAllHistorySize();
                for(int g=0;g<alldata.size();g++){
                    History his=new History();
                    his.setId(myandata.get(Integer.parseInt(alldata.get(g).getDhikrid())));
                    his.setDhikrid(spinnerdata.get(Integer.parseInt(alldata.get(g).getDhikrid())));

                    his.setCount(alldata.get(g).getCount());

                    his.setDate(alldata.get(g).getDate());
                    his.setTime(alldata.get(g).getTime());
                    contactsList.add(his);
                }




                LinearLayoutManager layoutManager = new LinearLayoutManager(v.getContext());
                historyrecycle.setLayoutManager(layoutManager);
                listAdapter = new HistoryAdaptor(contactsList, v.getContext());
                historyrecycle.setAdapter(listAdapter);

                //Load the date from the network or other resources
                //into the array list asynchronously


                AdView  mAdViewss = child.findViewById(R.id.ads);

                AdRequest adRequestss = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
                mAdViewss.loadAd(adRequestss);
                hissublayout.addView(child);
                hissublayout.setVisibility(View.VISIBLE);
                detaillayout.setVisibility(View.INVISIBLE);
                mainlayout.setVisibility(View.INVISIBLE);
                sublayout.setVisibility(View.INVISIBLE);
                pdflayout.setVisibility(View.INVISIBLE);

            }
        });
        hissublayout.setVisibility(View.INVISIBLE);
// OPTION 2: pack the file with the app
        /* "If you want to package the .csv file with the application and have it install on the internal storage when the app installs, create an assets folder in your project src/main folder (e.g., c:\myapp\app\src\main\assets\), and put the .csv file in there, then reference it like this in your activity:" (from the cited answer) */


        spinner.setSpinnerPopupHeight(800);
        spinner.setPadding(0,40,0,40);


        try {
            InputStream ins = getResources().openRawResource(
                    getResources().getIdentifier("asma",
                            "raw", getPackageName()));
            CSVReader reader = new CSVReader(new InputStreamReader(ins, "UTF-8"));
            String[] nextLine;

            spinnerdata.add("General");
            myandata.add("-");
            while ((nextLine = reader.readNext()) != null) {

                spinnerdata.add(nextLine[1]);
                myandata.add(nextLine[2]);

            }

            InputStream inss = getResources().openRawResource(
                    getResources().getIdentifier("dhikr",
                            "raw", getPackageName()));
            CSVReader readera = new CSVReader(new InputStreamReader(inss, "UTF-8"));
            String[] nextLineasma;
            while ((nextLineasma = readera.readNext()) != null) {

                spinnerdata.add(nextLineasma[2]);
                myandata.add(nextLineasma[3]);

            }


            spinner.setItems(spinnerdata);


            spinner.setOnSpinnerItemSelectedListener(new OnSpinnerItemSelectedListener<String>() {
                @Override
                public void onItemSelected(int oldIndex, @Nullable String oldItem, int newIndex, String newItem) {
                    if(oldIndex<0){
                        spinnerID=0+"";
                    }else{
                        spinnerID=oldIndex+"";
                    }
                    if(newIndex==0){
                        indexSpinner=0;
                    }else{
                        indexSpinner=newIndex;
                    }

Log.e("TAG",spinnerID+" SPINNER ID");
                    Log.e("TAG",indexSpinner+" INDEX SPINNER");


                    Date c = Calendar.getInstance().getTime();

                    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
                    String formattedDate = df.format(c);
                    history.setDate(formattedDate);
                    history.setClickreset(false);
                    history.setCount(countct.getText().toString());
                    con2=countct.getText().toString();
                    datatemp.clear();
                    if(newIndex>0 && newIndex<100){
                        try {
                            InputStream ins = MainActivity.this.getApplicationContext().getResources().openRawResource(
                                    MainActivity.this.getApplicationContext().getResources().getIdentifier("asma",
                                            "raw", MainActivity.this.getApplicationContext().getPackageName()));

                            CSVReader reader = new CSVReader(new InputStreamReader(ins, "UTF-8"));
                            List<String[]> allLines = reader.readAll();
                            // String no=viewHolder.no.getText().toString().trim();
                            DataModel dataModelss=new DataModel(allLines.get(newIndex-1)[0]+" ", ""+allLines.get(newIndex-1)[2], allLines.get(newIndex-1)[1],allLines.get(newIndex-1)[3],"asma");
                            // 2. Confirmation message
                            // create an alert builder
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                            // set the custom layout
                            final View customLayout = inflater.inflate(R.layout.custom_layout, null);
                            Button closebtn= customLayout.findViewById(R.id.confirm_button);
                            TextView myan_text= customLayout.findViewById(R.id.myan_text);
                            myan_text.setText(dataModelss.getArab().replace("n", System.getProperty("line.separator")));
                            TextView myande_text=customLayout.findViewById(R.id.myande_text);
                            myande_text.setText(dataModelss.getMyan().replace("n", System.getProperty("line.separator")));
                            TextView arab_text= customLayout.findViewById(R.id.arab_text);
                            arab_text.setText(dataModelss.getDetarab().replace("n", System.getProperty("line.separator")));
                            builder.setView(customLayout);


                            // create and show the alert dialog
                            AlertDialog dialog = builder.create();
                            dialog.show();
                            closebtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    spinner.dismiss();

                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }



                    }else if(newIndex>99){

                        int posi=newIndex-99;
                        try{
                            InputStream ins = MainActivity.this.getApplicationContext().getResources().openRawResource(
                                    MainActivity.this.getApplicationContext().getResources().getIdentifier("dhikr",
                                            "raw", MainActivity.this.getApplicationContext().getPackageName()));
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            LayoutInflater inflater = LayoutInflater.from(MainActivity.this);

                            CSVReader reader = new CSVReader(new InputStreamReader(ins, "UTF-8"));
                            List<String[]> allLines = reader.readAll();
                            DataModel dataModelss=new DataModel(allLines.get(posi-1)[1]+" ", ""+allLines.get(posi-1)[4],"","","asma");
                            final View customLayout = inflater.inflate(R.layout.custom_layout, null);
                            Button closebtn= customLayout.findViewById(R.id.confirm_button);
                            TextView myan_text= customLayout.findViewById(R.id.myan_text);
                            myan_text.setText(dataModelss.getNo().replace("n", System.getProperty("line.separator")));
                            TextView myande_text=customLayout.findViewById(R.id.myande_text);
                            myande_text.setText(dataModelss.getMyan().replace("n", System.getProperty("line.separator")));
                            TextView arab_text= customLayout.findViewById(R.id.arab_text);
                            arab_text.setVisibility(View.INVISIBLE);
                            View linese= customLayout.findViewById(R.id.linese);
                            linese.setVisibility(View.INVISIBLE);
                            builder.setView(customLayout);
                            // create and show the alert dialog
                            AlertDialog dialog = builder.create();
                            dialog.show();
                            closebtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    spinner.dismiss();
                                }
                            });


                        }catch(Exception e){
                            e.printStackTrace();
                        }

                    }
                    History dbhis=new History();
                    if(oldIndex==-1){
                        dbhis.setDate(history.getDate());
                        dbhis.setDhikrid("0");
                        dbhis.setClickreset(history.isClickreset());
                        dbhis.setCount(countct.getText().toString());
                        dbhis.setTime(timenow());

                    }else{
                        dbhis.setDate(history.getDate());
                        dbhis.setDhikrid(oldIndex+"");
                        dbhis.setClickreset(history.isClickreset());
                        dbhis.setCount(countct.getText().toString());
                        dbhis.setTime(timenow());

                    }
                    //TODO your background code
                    int dbcount=dbs.getHistoryCount();

                    if(dbcount==0){

                        if(Integer.parseInt(dbhis.getCount())>0){
                            dbs.addHistory(dbhis);
                        }

                    }else{
                        String datapoint;
                        if(oldIndex==-1){
                            datapoint="0";
                        }else{
                            datapoint=oldIndex+"";
                        }
                        ArrayList<History>sameDate=dbs.getHistoryofSameDate(datapoint);
                        if(sameDate.size()!=0) {
                            for(int i=0;i<sameDate.size();i++){

if(sameDate.get(i).isClickreset()==false){
    History datatoedit=new History();
    datatoedit.setId(sameDate.get(i).getId());
    datatoedit.setCount(countct.getText().toString());
    datatoedit.setDate(sameDate.get(i).getDate());
    datatoedit.setDhikrid(sameDate.get(i).getDhikrid());
    datatoedit.setClickreset(sameDate.get(i).isClickreset());
    //   datatoedit.setCount(countct.getText().toString());
    datatoedit.setTime(timenow());
    dbs.updateContact(datatoedit);
    dbs.close();
}

                            }
                        }else{
                            if(Integer.parseInt(dbhis.getCount())>0){
                                dbs.addHistory(dbhis);
                            }
                        }
                    }







                    history.setDhikrid(null);
                    history.setCount(null);
                    Log.e("TAG",datas+"HERESETTER");
                    if(newIndex==0){
                        indexSpinner=newIndex;
                        datas=sharedpreferences.getString("data", "0"); // getting String

                        KunmiViewFade_Animator animator = new KunmiViewFade_Animator(countct, new String[]{
                                datas+""});
                        countct.setText(datas);
                        animator.startAnimation();
                        history.setCount(datas);
                    }else{
                        indexSpinner=newIndex;
                        datas=sharedpreferences.getString("data"+newIndex, "0"); // getting String
                        KunmiViewFade_Animator animator = new KunmiViewFade_Animator(countct, new String[]{
                                datas+""});
                        countct.setText(datas);
                        animator.startAnimation();
                        history.setCount(datas);
                    }
                    history.setDhikrid(newIndex+"");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        navview.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                spinner.dismiss();
                switch (item.getItemId()){
                    case R.id.tasbih:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                        } else {
                            //deprecated in API 26
                            va.vibrate(50);
                        }

                        if(!(listView.getFooterViewsCount() >0)){
                            //      listView.addFooterView(friend_request_item);
                        }

                        detaillayout.setVisibility(View.INVISIBLE);
                        mainlayout.setVisibility(View.VISIBLE);
                        sublayout.setVisibility(View.INVISIBLE);
                        hissublayout.setVisibility(View.INVISIBLE);
                        pdflayout.setVisibility(View.INVISIBLE);
                        drawer.closeDrawers();
                        break;
                    case R.id.dhikr:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                        } else {
                            //deprecated in API 26
                            va.vibrate(50);
                        }
                        dataModels.clear();

                        titl.setText("Dhikr");
                        subtitl.setText("( ဇေကေလ် )");
                        try {
                            InputStream ins = getResources().openRawResource(
                                    getResources().getIdentifier("dhikr",
                                            "raw", getPackageName()));
                            CSVReader reader = new CSVReader(new InputStreamReader(ins, "UTF-8"));
                            String[] nextLine;
                            while ((nextLine = reader.readNext()) != null) {
                                dataModels.add(new DataModel(nextLine[0]+" ", ""+nextLine[3], nextLine[2],nextLine[1],"dhikr"));

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        adapter= new CustomAdaptor(dataModels,MainActivity.this);

                        if(!(listView.getFooterViewsCount() >0)){
                            LinearLayout myLayout = new LinearLayout(MainActivity.this);

                            Button myButton = new Button(MainActivity.this);
                            myButton.setBackgroundColor(0x1B1B1E);
                            myButton.setLayoutParams(new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    370));
                            myButton.setClickable(false);
                            myLayout.addView(myButton);

                            listView.addFooterView(myLayout);
                        }

                        listView.setAdapter(adapter);
                        pdflayout.setVisibility(View.INVISIBLE);
                        detaillayout.setVisibility(View.INVISIBLE);
                        mainlayout.setVisibility(View.INVISIBLE);
                        sublayout.setVisibility(View.VISIBLE);
                        hissublayout.setVisibility(View.INVISIBLE);
                        drawer.closeDrawers();
                        break;
                    case R.id.asma:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                        } else {
                            //deprecated in API 26
                            va.vibrate(50);
                        }
                        dataModels.clear();

                        titl.setText("Asma ul Husna");
                        subtitl.setText("( ဂုဏ်တော် ၉၉ ပါး )");

                        try {
                            InputStream ins = getResources().openRawResource(
                                    getResources().getIdentifier("asma",
                                            "raw", getPackageName()));
                            CSVReader reader = new CSVReader(new InputStreamReader(ins, "UTF-8"));
                            String[] nextLine;
                            while ((nextLine = reader.readNext()) != null) {
                                dataModels.add(new DataModel(nextLine[0]+" ", ""+nextLine[2], nextLine[1],nextLine[3],"asma"));

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        adapter= new CustomAdaptor(dataModels,MainActivity.this);


                        if(!(listView.getFooterViewsCount() >0)){

                            LinearLayout myLayouts = new LinearLayout(MainActivity.this);

                            Button myButtons = new Button(MainActivity.this);
                            myButtons.setLayoutParams(new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    370));
                            myButtons.setBackgroundColor(0x1B1B1E);
                            myLayouts.addView(myButtons);

                            myButtons.setClickable(false);
                            listView.addFooterView(myLayouts);
                        }
                        listView.setAdapter(adapter);
                        pdflayout.setVisibility(View.INVISIBLE);
                        detaillayout.setVisibility(View.INVISIBLE);
                        mainlayout.setVisibility(View.INVISIBLE);
                        sublayout.setVisibility(View.VISIBLE);
                        hissublayout.setVisibility(View.INVISIBLE);
                        sublayout.invalidate();
                        drawer.closeDrawers();

                        break;
                    case R.id.nafl:
                        if(!(listView.getFooterViewsCount() >0)){

                            LinearLayout myLayouts = new LinearLayout(MainActivity.this);

                            Button myButtons = new Button(MainActivity.this);
                            myButtons.setLayoutParams(new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    370));
                            myButtons.setBackgroundColor(0x1B1B1E);
                            myLayouts.addView(myButtons);

                            myButtons.setClickable(false);
                            listView.addFooterView(myLayouts);
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                        } else {
                            //deprecated in API 26
                            va.vibrate(50);
                        }

                        dataModels.clear();
                        titl.setText("Nafl Ibadat");
                        subtitl.setText("( နဖိလ်ကျင့်စဉ်များ )");
                        dataModels.add(new DataModel("01","ညဥ့်မြတ်တို့၏နဖိလ်ကျင့်စဉ်များ","صلاة نفل"," ","nafl"));
                        dataModels.add(new DataModel("02","ဆွလာသွတ် သတ်စ်ဗီးဟ် နမာဇ်","صلاة التسبيح"," ","nafl"));
                        adapter= new CustomAdaptor(dataModels,MainActivity.this);

                        listView.setAdapter(adapter);
                        pdflayout.setVisibility(View.INVISIBLE);
                        detaillayout.setVisibility(View.INVISIBLE);
                        mainlayout.setVisibility(View.INVISIBLE);
                        sublayout.setVisibility(View.VISIBLE);
                        hissublayout.setVisibility(View.INVISIBLE);
                        sublayout.invalidate();
                        drawer.closeDrawers();
                        break;
                    case R.id.rate:
                        drawer.closeDrawers();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                        } else {
                            //deprecated in API 26
                            va.vibrate(50);
                        }
                        showRateApp();
                        break;
                    case R.id.share:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                        } else {
                            //deprecated in API 26
                            va.vibrate(50);
                        }
                        //https://play.google.com/store/apps/details?id=com.kpshkk.tasbihmm
                        try {
                            Intent shareIntent = new Intent(Intent.ACTION_SEND);
                            shareIntent.setType("text/plain");
                            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "TasbihMM");
                            String shareMessage= "\nLet me recommend you this application\n\n";
                            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
                            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                            startActivity(Intent.createChooser(shareIntent, "Where to Share?"));
                        } catch(Exception e) {
                            //e.toString();
                        }
                        break;
                    case R.id.contact:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                        } else {
                            //deprecated in API 26
                            va.vibrate(50);
                        }
                        drawer.closeDrawers();

                        if(isNetworkAvailable()){
                            getPublicIP();

                        }else{
                            Toast.makeText(MainActivity.this,"No Internet Connection! \n အင်တာနက်ဖွင့်ရန်လိုအပ်ပါသည်။ ",Toast.LENGTH_LONG).show();
                        }
                        break;
                }
                return true;
            }
        });
        View header = navview.getHeaderView(0);
        crossclose = (ImageView) header.findViewById(R.id.crossclose);

        crossclose.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                drawer.closeDrawers();
                spinner.dismiss();
                return true;
            }
        });

        menubtn=findViewById(R.id.menubtn);
        menubtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                } else {
                    //deprecated in API 26
                    va.vibrate(50);
                }
                drawer.openDrawer(GravityCompat.START);
                spinner.dismiss();
            }
        });
        mainlayout=findViewById(R.id.mainlayout);
        mainlayout.setVisibility(View.VISIBLE);
        sublayout=findViewById(R.id.sublayout);
        sublayout.setVisibility(View.INVISIBLE);
        pdflayout.setVisibility(View.INVISIBLE);
        detaillayout=findViewById(R.id.detaillayout);
        detaillayout.setVisibility(View.INVISIBLE);
        hissublayout.setVisibility(View.VISIBLE);
        plusbtn=findViewById(R.id.plusbtn);
        minusbtn=findViewById(R.id.minusbtn);
        countct=findViewById(R.id.counttxt);
        resetbtn=findViewById(R.id.resetbtn);
        countct.setText(count+"");
        resetbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.dismiss();


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    va.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));

                } else {
                    //deprecated in API 26
                    va.vibrate(100);
                }
                didResetTapButton(v);
// 2. Confirmation message
                new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure to Reset?")

                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {

                                sDialog.dismissWithAnimation();
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                if(indexSpinner==0){
                                    editor.putString("data", 0+"");
                                }else{
                                    editor.putString("data"+indexSpinner, 0+"");
                                }
Log.e("TAG","RESET INDEX SPINNER: "+indexSpinner);

                                editor.commit();

                                KunmiViewFade_Animator animator = new KunmiViewFade_Animator(countct, new String[]{
                                        0+""});
                                animator.startAnimation();

                                Date c = Calendar.getInstance().getTime();

                                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
                                String formattedDate = df.format(c);
                                history.setDate(formattedDate);
                                history.setClickreset(true);


                                Log.e("TAG","RESET INDEX SPINNER SELECTED: "+spinner.getSelectedIndex());
                                Log.e("TAG","RESET GET COUNT : "+ countct.getText().toString());
                                int inde=0;
                                if((spinner.getSelectedIndex() != -1)){
                                    inde= spinner.getSelectedIndex();
                                }
                                Log.e("TAG",inde+"v");
                                int dbcount=dbs.getHistoryCount();

                                if(dbcount==0){

                                    if(Integer.parseInt(countct.getText().toString())>0){
                                        history.setCount(countct.getText().toString());
                                        dbs.addHistory(history);
                                    }


                                }else{

                                    ArrayList<History>sameDate=dbs.getHistoryofSameDate(inde+"");
                                    if(sameDate.size()!=0) {
                                        for(int i=0;i<sameDate.size();i++){

                                            if(sameDate.get(i).isClickreset()==false){
                                                History datatoedit=new History();
                                                datatoedit.setId(sameDate.get(i).getId());

                                                datatoedit.setDate(sameDate.get(i).getDate());
                                                datatoedit.setDhikrid(sameDate.get(i).getDhikrid());
                                                datatoedit.setClickreset(true);
                                                //   datatoedit.setCount(countct.getText().toString());
                                                datatoedit.setCount(countct.getText().toString());
                                                datatoedit.setTime(timenow());
                                                dbs.updateContact(datatoedit);
                                                dbs.close();
                                            }

                                        }
                                    }else{
                                        history.setCount(countct.getText().toString());
                                        history.setTime(timenow());
                                        if(Integer.parseInt(countct.getText().toString())>0){
                                            dbs.addHistory(history);
                                        }
                                    }
                                }












                            }
                        })
                        .setCancelButton("Cancel", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();


                count=0;
                history.setCount(count+"");



                if(indexSpinner==0){
                    datas=sharedpreferences.getString("data", "9999"); // getting String
                }else{

                    datas=sharedpreferences.getString("data"+indexSpinner, "9999"); // getting String
                }



            }
        });
        plusbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.dismiss();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    va.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));

                } else {
                    //deprecated in API 26
                    va.vibrate(100);
                }
                SharedPreferences.Editor editor = sharedpreferences.edit();


                plusbtn.setEnabled(false);
                didPlusTapButton(v);
                count=Integer.parseInt(countct.getText().toString())+1;
                KunmiViewFade_Animator animator = new KunmiViewFade_Animator(countct, new String[]{
                        count+""});
                animator.startAnimation();
                plusbtn.setEnabled(true);
                history.setCount(count+"");
                if(indexSpinner==0){
                    editor.putString("data", count+"");
                }else{
                    editor.putString("data"+indexSpinner, count+"");
                }


                editor.commit();
            }
        });
        minusbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.dismiss();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    va.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));

                } else {
                    //deprecated in API 26
                    va.vibrate(100);
                }
                count=Integer.parseInt(countct.getText().toString());
                SharedPreferences.Editor editor = sharedpreferences.edit();
                minusbtn.setEnabled(false);
                didMinusTapButton(v);
                if(count>0){
                    count=Integer.parseInt(countct.getText().toString())-1;
                    KunmiViewFade_Animator animator = new KunmiViewFade_Animator(countct, new String[]{
                            count+""});
                    animator.startAnimation();


                }
                if(count<1){
                    count=0;
                    KunmiViewFade_Animator animator = new KunmiViewFade_Animator(countct, new String[]{
                            count+""});
                    animator.startAnimation();

                }
                minusbtn.setEnabled(true);
                if(indexSpinner==0){
                    editor.putString("data", count+"");
                }else{
                    editor.putString("data"+indexSpinner, count+"");
                }
                history.setCount(count+"");
                editor.commit();
            }
        });

        KunmiViewFade_Animator animator = new KunmiViewFade_Animator(countct, new String[]{
                datas+""});
        animator.startAnimation();
        count= Integer.parseInt(datas);
        history.setCount(datas);
        history.setDhikrid("0");

        View child = getLayoutInflater().inflate(R.layout.content_main, null);
        AdView  mAdView = child.findViewById(R.id.ads);

        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        mAdView.loadAd(adRequest);

        titl= child.findViewById(R.id.title);
        subtitl=child.findViewById(R.id.subtitle);
        ImageView laymen=child.findViewById(R.id.menubtn);
        laymen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                } else {
                    //deprecated in API 26
                    va.vibrate(50);
                }
                spinner.dismiss();
                drawer.openDrawer(GravityCompat.START);
            }
        });
        RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
        relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        listView=(ListView)child.findViewById(R.id.list);
        listView.setItemsCanFocus(true);

        dataModels= new ArrayList<>();

        friend_request_item = LayoutInflater.from(MainActivity.this).inflate(R.layout.adslayout, null);
        friend_request_item.setMinimumHeight(350);
        if(!(listView.getFooterViewsCount() >0)){
            //      listView.addFooterView(friend_request_item);
        }
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
            @Override
            public void onScroll(AbsListView lw, final int firstVisibleItem,
                                 final int visibleItemCount, final int totalItemCount)
            {

                switch(lw.getId())
                {
                    case R.id.list:

                        // Make your calculation stuff here. You have all your
                        // needed info from the parameters of this function.

                        // Sample calculation to determine if the last
                        // item is fully visible.
                        final int lastItem = firstVisibleItem + visibleItemCount;

                        if(lastItem == totalItemCount)
                        {
                            if(preLast!=lastItem)
                            {
                                //to avoid multiple calls for last item


                                preLast = lastItem;
                            }
                        }
                }
            }
        });


        sublayout.addView(child,relativeParams);
    }

    public void didPlusTapButton(View view) {

        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.005, 1);
        myAnim.setInterpolator(interpolator);
        plusbtn.startAnimation(myAnim);

    }
    public void didResetTapButton(View view) {

        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.005, 1);
        myAnim.setInterpolator(interpolator);
        resetbtn.startAnimation(myAnim);

    }

    public void didMinusTapButton(View view) {

        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.005, 1);
        myAnim.setInterpolator(interpolator);
        minusbtn.startAnimation(myAnim);

    }
    public void pdfopen(String no){
        spinner.dismiss();
        pdflayout.removeAllViews();
        pdfview = getLayoutInflater().inflate(R.layout.pdflayout, null);
        hissublayout.setVisibility(View.INVISIBLE);
        pdflayout.setVisibility(View.VISIBLE);
        mainlayout.setVisibility(View.INVISIBLE);
        sublayout.setVisibility(View.INVISIBLE);




        PDFView pdfView=pdfview.findViewById(R.id.pdfview);

        if(no.toLowerCase().equals("01")){
            try{
                //Retrieve the resource ID:
                int resID = MainActivity.this.getResources().getIdentifier("naflibadat", "raw", MainActivity.this.getPackageName());

                if ( resID == 0 ) {  // the resource file does NOT exist!!
                    //Debug:

                    return;
                }

//Read the resource:
                InputStream inputStream = MainActivity.this.getResources().openRawResource(resID);

                pdfView.fromStream(inputStream)
                        .enableSwipe(true) // allows to block changing pages using swipe
                        .swipeHorizontal(false)
                        .enableDoubletap(true)
                        .defaultPage(0)

                        .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                        // spacing between pages in dp. To define spacing color, set view background
                        .pageFitPolicy(FitPolicy.WIDTH) // mode to fit pages in the view
                        .fitEachPage(true) // fit each page to the view, else smaller pages are scaled relative to largest page.
                        .nightMode(true)
                        .load();



            }catch (Exception ex){
                ex.printStackTrace();
            }


            // mode to fit pages in the view
        }else{
            try{
                //Retrieve the resource ID:
                int resID = MainActivity.this.getResources().getIdentifier("surahtusbihnamaz", "raw", MainActivity.this.getPackageName());

                if ( resID == 0 ) {  // the resource file does NOT exist!!
                    //Debug:

                    return;
                }

//Read the resource:
                InputStream inputStream = MainActivity.this.getResources().openRawResource(resID);

                pdfView.fromStream(inputStream)
                        .enableSwipe(true) // allows to block changing pages using swipe
                        .swipeHorizontal(false)
                        .enableDoubletap(true)
                        .defaultPage(0)

                        .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                        // spacing between pages in dp. To define spacing color, set view background
                        .pageFitPolicy(FitPolicy.WIDTH) // mode to fit pages in the view
                        .fitEachPage(true) // fit each page to the view, else smaller pages are scaled relative to largest page.
                        .nightMode(true)
                        .load();



            }catch (Exception ex){
                ex.printStackTrace();
            }

        }
        ImageButton  menubtntwo=pdfview.findViewById(R.id.menubtn);
        menubtntwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                } else {
                    //deprecated in API 26
                    va.vibrate(50);
                }
                spinner.dismiss();
                drawer.openDrawer(GravityCompat.START);
            }
        });
        ImageButton arrowtwo=pdfview.findViewById(R.id.arrowtwo);
        arrowtwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                } else {
                    //deprecated in API 26
                    va.vibrate(50);
                }

                spinner.dismiss();
                hissublayout.setVisibility(View.INVISIBLE);
                mainlayout.setVisibility(View.INVISIBLE);
                pdflayout.setVisibility(View.INVISIBLE);

                sublayout.setVisibility(View.VISIBLE);
            }
        });

        pdflayout.addView(pdfview);

    }

    public void detailOpen(String index2,String index4,String index5){
        spinner.dismiss();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

        } else {
            //deprecated in API 26
            va.vibrate(50);
        }
        detaillayout.removeAllViews();
        View child = getLayoutInflater().inflate(R.layout.detaillayout, null);
        AdView  mAdViewss = child.findViewById(R.id.ads2);
        AdRequest adRequestss = new AdRequest.Builder().build();
        mAdViewss.loadAd(adRequestss);
        ImageButton  menubtntwo=child.findViewById(R.id.menubtn);
        menubtntwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                } else {
                    //deprecated in API 26
                    va.vibrate(50);
                }
                spinner.dismiss();
                drawer.openDrawer(GravityCompat.START);
            }
        });
        ImageButton arrowtwo=child.findViewById(R.id.arrowtwo);
        arrowtwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.dismiss();
                dataModels.clear();
                Vibrator   va= (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

                } else {
                    //deprecated in API 26
                    va.vibrate(50);
                }
                titl.setText("Dhikr");
                subtitl.setText("( ဇေကေလ် )");
                try {
                    InputStream ins = getResources().openRawResource(
                            getResources().getIdentifier("dhikr",
                                    "raw", getPackageName()));
                    CSVReader reader = new CSVReader(new InputStreamReader(ins, "UTF-8"));
                    String[] nextLine;
                    while ((nextLine = reader.readNext()) != null) {
                        dataModels.add(new DataModel(nextLine[0]+" ", ""+nextLine[3], nextLine[2],nextLine[1],"dhikr"));

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                adapter= new CustomAdaptor(dataModels,MainActivity.this);

                listView.setAdapter(adapter);
                hissublayout.setVisibility(View.INVISIBLE);
                detaillayout.setVisibility(View.INVISIBLE);
                mainlayout.setVisibility(View.INVISIBLE);
                sublayout.setVisibility(View.VISIBLE);
            }
        });
        TextView titletwo=child.findViewById(R.id.title);
        titletwo.setText("Dhikr");

        TextView subtitletwo=child.findViewById(R.id.subtitle);
        subtitletwo.setText("( ဇေကေလ် )");
        TextView dua = child.findViewById(R.id.dua);
        dua.setText(index2);
        TextView promyan= child.findViewById(R.id.promyan);
        promyan.setText(index4);
        TextView reading= child.findViewById(R.id.reading);
        reading.setText(index5);
        detaillayout.addView(child);
        detaillayout.setVisibility(View.VISIBLE);

        mainlayout.setVisibility(View.INVISIBLE);
        sublayout.setVisibility(View.INVISIBLE);
        hissublayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        spinner.dismiss();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            va.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));

        } else {
            //deprecated in API 26
            va.vibrate(50);
        }

        if(drawer.isDrawerOpen((GravityCompat.START))){
            drawer.closeDrawers();
        }
        if(sublayout.isShown()){
            sublayout.setVisibility(View.INVISIBLE);
            mainlayout.setVisibility(View.VISIBLE);
        }

        else  if(hissublayout.isShown()){
            hissublayout.setVisibility(View.INVISIBLE);
            mainlayout.setVisibility(View.VISIBLE);
        }else if(pdflayout.isShown()){
            pdflayout.setVisibility(View.INVISIBLE);
            sublayout.setVisibility(View.VISIBLE);
        }



        else{
            if(detaillayout.isShown()){

                detaillayout.removeAllViews();
                dataModels.clear();

                titl.setText("Dhikr");
                subtitl.setText("( ဇေကေလ် )");
                try {
                    InputStream ins = getResources().openRawResource(
                            getResources().getIdentifier("dhikr",
                                    "raw", getPackageName()));
                    CSVReader reader = new CSVReader(new InputStreamReader(ins, "UTF-8"));
                    String[] nextLine;
                    while ((nextLine = reader.readNext()) != null) {
                        dataModels.add(new DataModel(nextLine[0]+" ", ""+nextLine[3], nextLine[2],nextLine[1],"dhikr"));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                adapter= new CustomAdaptor(dataModels,MainActivity.this);

                listView.setAdapter(adapter);
                detaillayout.setVisibility(View.INVISIBLE);
                hissublayout.setVisibility(View.INVISIBLE);
                mainlayout.setVisibility(View.INVISIBLE);
                sublayout.setVisibility(View.VISIBLE);
            }else{
                super.onBackPressed();
            }

            Date c = Calendar.getInstance().getTime();

            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
            String formattedDate = df.format(c);
            history.setDate(formattedDate);
            history.setClickreset(false);







        }

    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void getPublicIP() {
        ArrayList<String>urls=new ArrayList<>();
        ProgressDialog progressBar = new ProgressDialog(this);
        progressBar.setMessage("Loading...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressBar.show();//displays the progress bar
        new Thread(new Runnable(){
            public void run(){

                try {
                    // Create a URL for the desired page
                    URL url = new URL("https://api.ipify.org/"); //My text file location
                    //First open the connection
                    HttpURLConnection conn=(HttpURLConnection) url.openConnection();
                    conn.setConnectTimeout(60000); // timing out in a minute

                    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                    String str;
                    while ((str = in.readLine()) != null) {
                        urls.add(str);
                    }
                    in.close();
                } catch (Exception e) {
                    Log.d("MyTag",e.toString());
                }

                //since we are in background thread, to post results we have to go back to ui thread. do the following for that

                MainActivity.this.runOnUiThread(new Runnable(){
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    public void run(){
                        try {
                            progressBar.dismiss();
                            WifiManager manager = (WifiManager) MainActivity.this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                            WifiInfo info = manager.getConnectionInfo();
                            String address = info.getMacAddress();

                            try {



                                String dat="Manufacture: " + Build.MANUFACTURER +" MODEL: " + Build.MODEL+"SERIAL: " + Build.SERIAL+"\n"
                                        +"SDK  " + Build.VERSION.SDK+"\n"+"BASE: " + Build.VERSION_CODES.BASE+"\n"+"Version Code: " + Build.VERSION.RELEASE
                                        +"\n"+"BRAND " + Build.BRAND+"Manufacture: "+"\n" + Build.MANUFACTURER+"\n"+"Public IP:"+urls.get(0)+"\n"+"Private IP:"+info.getIpAddress()+"\n"+"UNIQUE KEY: " +  address;
                                String data;
                                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    // only for gingerbread and newer versions
                                    data=Base64.getEncoder().encodeToString( dat.getBytes());
                                }else{
                                    data= UUID.randomUUID().toString();
                                }



                                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                                emailIntent.setType("text/plain");
                                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{  "hkk.technology@gmail.com"});
                                emailIntent.putExtra(Intent.EXTRA_CC,new String[]{"vocaloidnationmyanmar.vnm@gmail.com"});
                                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Contact Us Form (TusbihMM)");
                                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "[Add Message Here] \n \n \n \n \n \n \n"+"___________________________ \n" +
                                        "\n \n \n \n \n \n \n (Please Do Not Delete)Customer Code :  "+data );



                                emailIntent.setType("message/rfc822");
                                try {
                                    startActivity(Intent.createChooser(emailIntent,
                                            "Send email using..."));
                                } catch (android.content.ActivityNotFoundException ex) {
                                    Toast.makeText(MainActivity.this,
                                            "No email clients installed.",
                                            Toast.LENGTH_SHORT).show();
                                }

                            }catch (Exception ex){
                                ex.printStackTrace();
                            }



                        }
                        catch (Exception e){

                        }
                    }
                });

            }
        }).start();

    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void scheduleNotification() {


        Intent intent = new Intent(this, SendDataService.class);
        startService(intent);

    }
    public String timenow(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
        ///    System.out.println( sdf.format(cal.getTime()) );
    }
    public String datenow(){
        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        return formattedDate;
    }


}